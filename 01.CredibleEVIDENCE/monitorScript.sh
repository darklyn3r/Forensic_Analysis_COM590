#!/bin/bash

# Fecha actual
FECHA=$(date +'%d/%m/%Y %H:%M:%S')
echo "La fecha actual es: $FECHA"

# Declaración de la función
EstadoServicio() {
    systemctl --quiet is-active $1
    if [ $? -eq 0 ]; then
        echo "- El servicio $1 está activo"
    else
        echo "- El servicio $1 está inactivo"
    fi
}


# Servicios a chequear (Podemos agregar todos los que deseemos)
# El servicio de Sincronización de tiempo de red
EstadoServicio systemd-timesyncd.service

# El servicio de Administración de discos
EstadoServicio udisks2.service

# Un servicio DHCP
EstadoServicio dhcpcd@enp4s0.service

# El servicio de Administración de red
EstadoServicio NetworkManager.service

# El servicio de Firewall
EstadoServicio iptables.service

# El servicio de Secure Shell
EstadoServicio sshd.service
