#!/bin/bash
 
DEBUG=0   # 0 no debug 1 test 2 debug

# Variables Globales al archivo

awk=`which awk`
lsbrel=`which lsb_release`

# Identificador de version
if [ -x $lsbrel ] ; then
   distro=`$lsbrel -i | $awk '{print $(NF)}'`
   nombre=`$lsbrel -c | $awk '{print $(NF)}'`
   release=`$lsbrel -r | $awk '{print $(NF)}' | cut -f1 -d"." `
else
   if [ -f /etc/redhat-release ] ; then
      distro=`awk '{print $1}'  /etc/redhat-release`
      release=`awk '{print $3}'  /etc/redhat-release`
      nombre=`awk '{print $4}'  /etc/redhat-release  | $awk '{print $(NF)}' | sed -e "s/\"//g" -e 's/(//g' -e 's/)//g'`
      if [[ "$release" == "release" ]] ; then
         release=`awk '{print $4}'  /etc/redhat-release `
         nombre=`awk '{print $5}'  /etc/redhat-release | $awk '{print $(NF)}' | sed -e "s/\"//g" -e 's/(//g' -e 's/)//g'`
      fi
   elif [ -f /etc/debian_version ] ; then
      distro=`awk '{print $1}'  /etc/issue`
      if [[ "$distro" == "Debian" ]] ; then
         release=`awk '{print $3}'  /etc/issue`
         nombre=`grep "VERSION=" /etc/os-release | sed -e "s/\"//g" -e 's/(//g' -e 's/)//g' | awk '{print $2}'`
      else
         release=`awk '{print $2}'  /etc/issue`
         nombre=`awk '{print $1}' /etc/debian_version | cut -f1 -d'/'`
      fi
   fi
fi
 
# Versiones
version=`echo $release | cut -f1 -d'.'`
case $distro in
   Debian|Ubuntu|LinuxMint|KaliLinux|ParrotSecurity)
family="debian" ; package="apt"  
       ;;
   ArchLinux|ManjaroLinux|BlackArch|ArcoLinux|RebornOS)
family="Arch Linux" ; package="pacman"
       ;;
   CentOS|RedHat|Fedora|Scientific|RedHatEnterpriseServer)
         family="redhat" ; package="yum"  
       ;;
esac

#echo -e "\n█▓▒░ Verifying distribution ░▒▓█"

echo -ne '█▓▒░ Verifying distribution ░▒▓█  [####                              ]  10% \r'
sleep 0.5
echo -ne '█▓▒░ Verifying distribution ░▒▓█  [########                          ]  20% \r'
sleep 0.8
echo -ne '█▓▒░ Verifying distribution ░▒▓█  [#################                 ]  42% \r'
sleep 0.5
echo -ne '█▓▒░ Verifying distribution ░▒▓█  [#########################         ]  66% \r'
sleep 0.4
echo -ne '█▓▒░ Verifying distribution ░▒▓█  [##################################]  100% \r'
echo -ne '\n'


echo -e "----------------------------------------------------------------------------------------------------------"
echo "Its distribution is $distro and is based on $family, so the $package package manager will be used.. "
echo -e "----------------------------------------------------------------------------------------------------------"

# Salida
exit 0
